# PostmanAPITesting

## Name of Projects

- POST - Data Driven Project (.csv/JSON File Imports)

- Newman Export Report


## Description
Postman supports CSV and JSON files to get data for the test scripts. The data-driven approach is useful when we need to execute a test with multiple sets of Data. Also, modifying or adding any data fields will just need updating the data files which is easier than going to test script and updating test data.

## Steps to Follow

**- POST - Data Driven Project (.csv/JSON File Imports)**

1.	To refer/create (Post) the column name and column job from the external data .csv file we need to set the variables under the ‘Pre-request Script’ tab in Postman. Doing so helps to read the values from the variables. Created the variables under the collection environment.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20074102.png?ref_type=heads)
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20065201.png?ref_type=heads)

2.	Created .csv file having these variables and their values.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20065434.png?ref_type=heads)

3.	Next to go to the Collection Runner, import this .csv data file and run the collection.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20065832.png?ref_type=heads)

4.	The response is shown below.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20070131.png?ref_type=heads)

5.	Similarly, we can import the JSON file data and run the collection.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20070533.png?ref_type=heads)

**- Newman Export Report**

1.	Click on ‘Collections’ in Postman and right click on the three dots under Rest_API collection and select export to export the collection. Save the export under ‘Newman’ folder in file explorer.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20073319.png?ref_type=heads)

2.	Similarly, click on ‘Environments’ in Postman and right click on the three dots under ReqResEnvironment and select export to export the environment files. Save the export under ‘Newman’ folder in file explorer.
![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20074102.png?ref_type=heads)

3.	Open the command prompt window and execute the following commands to get the html reports.

Newman Commannd syntax :
newman run {location of postman collection \ name of the collection } -d{location of the data file and name of the data file } -e {location of postman environment  \ name of the environment } --reporters {name of the reporter} --reporter-{ name of the reporter }-export {location where you want save your out put html report}

![Alt text](https://gitlab.com/testautomation5501512/postman_newmanapitesting_report/-/blob/main/GitLab%20Screenshots/Screenshot%202024-03-28%20075259.png?ref_type=heads)
